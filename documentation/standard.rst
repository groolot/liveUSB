Présentation du système |project|
=================================


**Clef USB démarrable**, support de l'**enseignement de la spécialité ISN en Terminale S** (notamment, mais peut être construite pour d'autres usages : Physique/Chimie, Mathématiques, STI2D, etc.).

Contient une version adaptée de **Debian GNU/Linux**.

C'est une clef **persistante** (les modifications sont conservées entre les redémarrages).

Logiciels installés
-------------------

Voir la :file:`liste des paquets installés <SYSTEME.rst#liste-des-paquets-install%C3%A9s>`.

.. include:: ../SYSTEME.rst
   :start-after: Liste des paquets installés

Comparaison avec ClefAgreg/Clef ISN
-----------------------------------

Il existe déjà une `ClefAgreg/Clef ISN <http://clefagreg.dnsalias.org/8.0/>`_ pour l'agrégation de mathématiques, pour l'informatique en prépa, pour l'ISN. Cette clef est `référencée <https://wiki.inria.fr/sciencinfolycee/ClefISN>`_ sur le site du SILO. Nous l'avons testée (voir l':issue:`31`) et elle fonctionne bien, sauf sur l'un de nos ordinateurs, qui est plutôt récent. **Mais surtout, elle nous semble plus compliquée à configurer et à mettre à jour**.

Téléchargement et utilisation du système |project|
==================================================


Pour utiliser un système déjà configuré et testé (méthode la plus simple)
-------------------------------------------------------------------------
Téléchargement
^^^^^^^^^^^^^^
La dernière image système validée et construite de |project| v\ |version| est :img:`.img`.

Vous pouvez consulter les autres constructions en visitant `liveUSB <http://liveusb.groolot.net>`_.

Documentations
^^^^^^^^^^^^^^
- :doc:`distributeur` : des instructions pour "recopier" le système sur une clef USB.
- :doc:`utilisateur` : comment démarrer sur la clef |project|, et ensuite comment utiliser le système |project|, *etc.*

Pour configurer le système pour d'autres besoins (méthode plus complète)
------------------------------------------------------------------------
Pour configurer la construction d'une autre image système, voir la :doc:`constructeur`.

Captures d'écran
================


Généralités
-----------
Voir :ref:`bootmenu` et :ref:`desktop`.

.. _bootmenu:

.. figure:: presentations/data/screenshot-boot-menu.png
   :scale: 70
   :alt: Menu de démarrage de la clef (basé sur SYSLINUX)

   Menu de démarrage de la clef (basé sur SYSLINUX)

.. _desktop:

.. figure:: presentations/data/screenshot-desktop.png
   :alt: Bureau et gestionnaire de fenêtres (Gnome3)

   Bureau et gestionnaire de fenêtres (Gnome3)


Exemples d'applications préinstallées (ici en version |version|\ )
-----------------------------------------------------------------   

.. image:: presentations/data/screenshot-applications-1.png
   :scale: 50
.. image:: presentations/data/screenshot-applications-2.png
   :scale: 50
.. image:: presentations/data/screenshot-applications-3.png
   :scale: 50
.. image:: presentations/data/screenshot-applications-4.png
   :scale: 50

