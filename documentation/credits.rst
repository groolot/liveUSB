Contributions
#############

Documentations
 * Vincent LABBÉ <vincent.labbe@ac-nantes.fr>

Tests et validations
 * Vincent LABBÉ <vincent.labbe@ac-nantes.fr>
 * Dimitri TRICARD <https://git.framasoft.org/u/D.Tricard>
  
