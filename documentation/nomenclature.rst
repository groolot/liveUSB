Nomenclature
############
Les lignes de commande commençant par ``$`` doivent être saisies par l'utilisateur lambda, exemple :

.. highlight:: none
::

  $ cd ~
  $ ip link show


Les lignes de commande commençant par ``#`` doivent être saisies par le super utilisateur, ou en tant qu'utilisateur avec élévation éphémère de pouvoir (``sudo``), exemple :

.. highlight:: none
::

  # touch /etc/shadow
  # rm -rf /tmp/*

ou l'équivalent avec ``sudo`` :

.. highlight:: none
::

  $ sudo touch /etc/shadow
  $ sudo rm -rf /tmp/*


Les lignes ne commençant pas par ``$`` ou ``#`` **ne doivent pas être saisies** car elles sont
les affichages de sortie des commandes précédentes, exemple :

.. highlight:: none
::

  $ cat /etc/hostname
  ISNconstructeur
  # ls /var
  backups  cache  games  lib  local  lock  log  mail  opt  run  spool  tmp
