Documentation constructeur
##########################

Prologue
========
Cette documentation est une simplification de la documentation officielle de `live-build <http://debian-live.alioth.debian.org/live-manual/unstable/manual/html/live-manual.en.html>`_
et est adaptée à la configuration fournie par ce projet.

Nomenclature
------------

Voir :doc:`nomenclature` pour des explications sur la nomenclature concernant les lignes de commande.

Numérotation des versions
-------------------------
Nous nous basons sur le principe de `numérotation sémantique des versions <http://www.semver.org>`_ proposé par `Tom Preston Werner <http://tom.preston-werner.com/>`_.


Mécanisme général d'une **construction**
========================================
La construction d'une clef USB *bootable* requiert d'assurer les étapes suivantes :

* **Installation** des :ref:`prerequis` sur votre système de construction
* :ref:`configurationinitiale`
* :ref:`configurationspecifique` (RC ou entière)
* :ref:`construction`

.. note::
   **Les 3 stades de la construction**

   * la construction d'une **intermédiaire** qui mènera à une RC (développement)
   * la construction d'une **RC** (*release candidate*) qui mènera à une distribuable (test et validation)
   * la construction d'une **version distribuable** (mise en production)

   Quel que soit le stade de la construction, le mécanisme est toujours le même. La variance d'un stade à l'autre correspond à la position dans l'arbre des configurations figées (sur quel ``tag`` nous plaçons-nous pour réaliser la construction).

Mise en œuvre de la construction
================================
`Voir la vidéo de démonstration <https://www.youtube.com/watch?v=mSmJwO6gATY>`_ dont les étapes sont détaillées ci-après.

.. include:: prerequis.rst


Super-utilisateur ?
^^^^^^^^^^^^^^^^^^
La commande suivante permet de connaître le nom de l'utilisateur que nous sommes :

.. highlight:: none
::

  # whoami
  root

si l'utilisateur n'est pas ``root``, alors il faut s'assurer que nous sommes un utilisateur faisant partie du groupe ``sudo`` :

.. highlight:: none
::

  $ groups
  lambda cdrom floppy sudo audio video plugdev ...

Si dans la sortie de la commande ``groups`` vous n'appercevez pas le mot clef ``sudo``, c'est que vous ne faites pas partie du groupe ``sudo``. Dans ce cas, vérifiez avec l'administrateur de votre système afin d'obtenir un compte vous permettant de réaliser en tant que super-utilisateur les commandes suivantes : ``make``, ``lb`` et ``apt-get``.

Installer les logiciels nécessaires
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. highlight:: none
::

   # apt-get update && apt-get install git live-build debootstrap make

ou avec ``sudo``

.. highlight:: none
::

   $ sudo apt-get update && sudo apt-get install git live-build debootstrap make

.. _configurationinitiale:
   
Récupération de la configuration initiale et des outils accessoires
-------------------------------------------------------------------

.. highlight:: none
::

   $ cd ~
   $ git clone https://framagit.org/groolot/liveUSB.git
   $ cd liveUSB
   $ ls
   auto automate_release burn config credentials dist documentation Makefile README.rst SYSTEM.rst

.. _configurationspecifique:

Choix de la configuration spécifique
------------------------------------
Lister les configurations disponibles
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. highlight:: none
::

   $ git tag
   v0.1
   v0.1.1
   v0.1.2
   v0.1.3
   ...
   v0.1.5-jessie


Exemple de construction de l'image système dans sa version ``v0.1.3``
---------------------------------------------------------------------

.. highlight:: none
::

   # git checkout v0.1.3
   # make build

ou bien avec ``sudo``

.. highlight:: none
::

   $ git checkout v0.1.3
   $ sudo make build


À l'issue de la construction, l'image système se trouve disponible dans le répertoire courant portant le nom ``liveUSB-v0.1.3-amd64.img`` (ou tout autre numéro de version en fonction du choix de la configuration fait au préalable).
