Documentation |project| v\ |version|
##################################
:Author: |author|
:Release: |version|
:Date: |today|
:Copyright: GNU Free Documentation License 1.3 with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts

.. toctree::
   :glob:
   :numbered:
   :maxdepth: 2

   standard
   utilisateur
   distributeur
   constructeur
   contributeur
   nondebian
   structuredetravail
