Documentation contributeur
##########################

.. _dysfonctionnement:

Vous rencontrez des dysfonctionnements
======================================
.. _usage:

À l'usage de la clef |project|
------------------------------
#. Assurez-vous d'avoir lu :doc:`utilisateur`
#. Connectez-vous à `FramaGit <https://git.framasoft.org>`_ (`ouvrir un compte <https://git.framasoft.org/users/sign_in>`_ si besoin)
#. Rendez-vous dans l'`espace de gestion des incidents <https://git.framasoft.org/groolot/liveUSB/issues>`_
#. Recherchez dans l'ensemble des *issues* ouvertes et fermées si quelqu'un n'a pas déjà rencontré votre dysfonctionnement (utilisez le champ de recherche en haut à droite de la liste pour effectuer une `recherche plein texte <https://fr.wikipedia.org/wiki/Recherche_plein_texte>`_)
#. Si à ce stade vous n'avez trouvé d'informations satisfaisantes alors vous allez devoir rédiger une nouvelle *issue* afin de nous rendre compte de votre situation et du dysfonctionnement :

   #. Accédez à la `page de création <https://git.framasoft.org/groolot/liveUSB/issues/new?issue>`_ d'une nouvelle *issue*
   #. Renseignez le champ ``Title`` d'une phrase très courte décrivant le problème comme par exemple : *"le pointeur de souris ne s'affiche"*
   #. Renseignez le champ ``Description`` d'une description détaillée décrivant le problème, avec les éléments minimum suivants :

      * le modèle de votre machine
      * la quantité de mémoire vive (``RAM`` exprimée en ``Go``)
      * le modèle du CPU (ex : ``Dual Core i5-2450M@2.4GHz``)
      * la version de |project| touchée par ce dysfonctionnement
      * la description de la méthode pour reproduire le dysfonctionnement
      * une proposition de correction ou d'amélioration (optionnel)

   #. Renseigner le champ ``Labels`` à :
   
      * ``err: boot`` si le dysfonctionnement empêche le démarrage du système |project|
      * ``err: system`` si le dysfonctionnement survient une fois le système |project| démarré
      
   #. Valider la création de cette nouvelle *issue* en appuyant sur le bouton ``Submit issue``

.. warning::
   **NE PAS RENSEIGNER** les champs ``Assignee`` et ``Milestone``


.. _distribution:
   
Lors de la distribution d'une clef |project|
--------------------------------------------
#. Assurez-vous d'avoir lu :doc:`distributeur`
#. Connectez-vous à `FramaGit <https://git.framasoft.org>`_ (`ouvrir un compte <https://git.framasoft.org/users/sign_in>`_ si besoin)
#. Rendez-vous dans l'`espace de gestion des incidents <https://git.framasoft.org/groolot/liveUSB/issues>`_
#. Recherchez dans l'ensemble des *issues* ouvertes et fermées si quelqu'un n'a pas déjà rencontré votre dysfonctionnement (utilisez le champ de recherche en haut à droite de la liste pour effectuer une `recherche plein texte <https://fr.wikipedia.org/wiki/Recherche_plein_texte>`_)
#. Si à ce stade vous n'avez trouvé d'informations satisfaisantes alors vous allez devoir rédiger une nouvelle *issue* afin de nous rendre compte de votre situation et du dysfonctionnement :

   #. Accédez à la `page de création <https://git.framasoft.org/groolot/liveUSB/issues/new?issue>`_ d'une nouvelle *issue*
   #. Renseignez le champ ``Title`` d'une phrase très courte décrivant le problème comme par exemple : *"la partition de persistance ne se créée pas"*
   #. Renseignez le champ ``Description`` d'une description détaillée décrivant le problème, avec les éléments minimum suivants :

      * le modèle de votre machine
      * la quantité de mémoire vive (``RAM`` exprimée en ``Go``)
      * le modèle du CPU (ex : ``Dual Core i5-2450M@2.4GHz``)
      * la version de |project| touchée par ce dysfonctionnement
      * la description de la méthode pour reproduire le dysfonctionnement
      * une proposition de correction ou d'amélioration (optionnel)

   #. Renseigner le champ ``Labels`` à ``err: distribuer``
   #. Valider la création de cette nouvelle *issue* en appuyant sur le bouton ``Submit issue``

.. warning::
   **NE PAS RENSEIGNER** les champs ``Assignee`` et ``Milestone``


.. _construction:
   
Lors de la construction d'un système |project|
----------------------------------------------
#. Assurez-vous d'avoir lu :doc:`constructeur`
#. Connectez-vous à `FramaGit <https://git.framasoft.org>`_ (`ouvrir un compte <https://git.framasoft.org/users/sign_in>`_ si besoin)
#. Rendez-vous dans l'`espace de gestion des incidents <https://git.framasoft.org/groolot/liveUSB/issues>`_
#. Recherchez dans l'ensemble des *issues* ouvertes et fermées si quelqu'un n'a pas déjà rencontré votre dysfonctionnement (utilisez le champ de recherche en haut à droite de la liste pour effectuer une `recherche plein texte <https://fr.wikipedia.org/wiki/Recherche_plein_texte>`_)
#. Si à ce stade vous n'avez trouvé d'informations satisfaisantes alors vous allez devoir rédiger une nouvelle *issue* afin de nous rendre compte de votre situation et du dysfonctionnement :

   #. Accédez à la `page de création <https://git.framasoft.org/groolot/liveUSB/issues/new?issue>`_ d'une nouvelle *issue*
   #. Renseignez le champ ``Title`` d'une phrase très courte décrivant le problème comme par exemple : *"la partition de persistance ne se créée pas"*
   #. Renseignez le champ ``Description`` d'une description détaillée décrivant le problème, avec les éléments minimum suivants :

      * le modèle de votre machine
      * la quantité de mémoire vive (``RAM`` exprimée en ``Go``)
      * le modèle du CPU (ex : ``Dual Core i5-2450M@2.4GHz``)
      * la version de |project| touchée par ce dysfonctionnement
      * la description de la méthode pour reproduire le dysfonctionnement
      * une proposition de correction ou d'amélioration (optionnel)

   #. Renseigner le champ ``Labels`` à ``err: build``
   #. Valider la création de cette nouvelle *issue* en appuyant sur le bouton ``Submit issue``

.. warning::
   **NE PAS RENSEIGNER** les champs ``Assignee`` et ``Milestone``


.. _documentation:

Vous ne trouvez pas la documentation adaptée à votre besoin
===========================================================
#. Assurez-vous d'avoir lu :doc:`index`
#. Connectez-vous à `FramaGit <https://git.framasoft.org>`_ (`ouvrir un compte <https://git.framasoft.org/users/sign_in>`_ si besoin)
#. Rendez-vous dans l'`espace de gestion des incidents <https://git.framasoft.org/groolot/liveUSB/issues>`_
#. Recherchez dans l'ensemble des *issues* ouvertes et fermées si quelqu'un n'a pas déjà évoqué ce manque documentaire (utilisez le champ de recherche en haut à droite de la liste pour effectuer une `recherche plein texte <https://fr.wikipedia.org/wiki/Recherche_plein_texte>`_)
#. Si à ce stade vous n'avez trouvé d'informations satisfaisantes alors vous allez devoir rédiger une nouvelle *issue* afin de nous rendre compte de votre situation et du manque documentaire :

   #. Accédez à la `page de création <https://git.framasoft.org/groolot/liveUSB/issues/new?issue>`_ d'une nouvelle *issue*
   #. Renseignez le champ ``Title`` d'une phrase très courte décrivant le problème comme par exemple : *"pas de documentation sur les participantsd au projet"*
   #. Renseignez le champ ``Description`` d'une description détaillée décrivant le manque documentaire, avec les éléments minimum suivants :

      * le modèle de votre machine
      * la quantité de mémoire vive (``RAM`` exprimée en ``Go``)
      * le modèle du CPU (ex : ``Dual Core i5-2450M@2.4GHz``)
      * la version de |project| touchée par ce manque documentaire
      * la description de la documentation absente
      * où placeriez-vous cette documentation ?
      * une proposition de correction ou d'amélioration (optionnel)

   #. Renseigner le champ ``Labels`` à ``étape: documentation``
   #. Valider la création de cette nouvelle *issue* en appuyant sur le bouton ``Submit issue``

.. warning::
   **NE PAS RENSEIGNER** les champs ``Assignee`` et ``Milestone``


.. _apports:

Vous voulez apporter votre aide, votre savoir-faire ou votre expertise
======================================================================
Lisez avant tout le diagramme :doc:`structuredetravail` afin de vous assurer du nom du rôle ou de la tâche que vous proposez.

.. _redigerdocumentation:

Vous voulez participer à la rédaction de la documentation
---------------------------------------------------------
#. Assurez-vous d'avoir lu :doc:`structuredetravail` et :ref:`documentation`
#. Connectez-vous à `FramaGit <https://git.framasoft.org>`_ (`ouvrir un compte <https://git.framasoft.org/users/sign_in>`_ si besoin)
#. Rendez-vous dans l'`espace de gestion des incidents <https://git.framasoft.org/groolot/liveUSB/issues>`_
#. Accédez à la `page de création <https://git.framasoft.org/groolot/liveUSB/issues/new?issue>`_ d'une nouvelle *issue*
#. Renseignez le champ ``Title`` d'une phrase très courte décrivant votre proposition : *"documenter l'étape de test d'une version RC"*
#. Renseignez le champ ``Description`` d'une description détaillée décrivant votre proposition documentaire, avec les éléments minimum suivants :

   * le modèle de votre machine
   * la quantité de mémoire vive (``RAM`` exprimée en ``Go``)
   * le modèle du CPU (ex : ``Dual Core i5-2450M@2.4GHz``)
   * la version de |project| touchée par cette proposition
   * l'ébauche de la documentation envisagée (plan et léger contenu)
   * où placeriez-vous cette documentation ?

#. Renseigner le champ ``Labels`` à ``étape: documentation``
#. Valider la création de cette nouvelle *issue* en appuyant sur le bouton ``Submit issue``

.. warning::
   **NE PAS RENSEIGNER** les champs ``Assignee`` et ``Milestone``



.. _ajoutlogiciel:

Il faut améliorer la collection logicielle
------------------------------------------
#. Assurez-vous d'avoir lu :doc:`structuredetravail` et la liste des logiciels installés dans :doc:`standard`
#. Connectez-vous à `FramaGit <https://git.framasoft.org>`_ (`ouvrir un compte <https://git.framasoft.org/users/sign_in>`_ si besoin)
#. Rendez-vous dans l'`espace de gestion des incidents <https://git.framasoft.org/groolot/liveUSB/issues>`_
#. Accédez à la `page de création <https://git.framasoft.org/groolot/liveUSB/issues/new?issue>`_ d'une nouvelle *issue*
#. Renseignez le champ ``Title`` d'une phrase très courte décrivant votre proposition : *"ajouter le logiciel '0A.D.'"* ou bien *"supprimer le logiciel 'ffmpeg'"*
#. Renseignez le champ ``Description`` d'une description détaillée décrivant votre proposition, avec les éléments minimum suivants :

   * le modèle de votre machine
   * la quantité de mémoire vive (``RAM`` exprimée en ``Go``)
   * le modèle du CPU (ex : ``Dual Core i5-2450M@2.4GHz``)
   * la version de |project| touchée par cette proposition
   * le nom exact du paquet logiciel dans `Debian <https://www.debian.org/distrib/packages#view>`_ distribution |distribution|
   * à quelle catégorie de logiciels celui-ci se rapporte-t-il ? (voir la liste des logiciels installés dans :doc:`standard`)

#. Renseigner le champ ``Labels`` à ``étape: configurer`` **et** ajouter le label :

   * ``pkg: manquant`` si vous prpoposez un **ajout**
   * ``pkg: à supprimer`` si vous prpoposez une **suppression**

#. Valider la création de cette nouvelle *issue* en appuyant sur le bouton ``Submit issue``

.. warning::
   **NE PAS RENSEIGNER** les champs ``Assignee`` et ``Milestone``


.. _esthetique:

Il faut vraiment améliorer l'aspect esthétique du système |project|
-------------------------------------------------------------------
#. Assurez-vous d'avoir lu :doc:`structuredetravail`
#. Connectez-vous à `FramaGit <https://git.framasoft.org>`_ (`ouvrir un compte <https://git.framasoft.org/users/sign_in>`_ si besoin)
#. Rendez-vous dans l'`espace de gestion des incidents <https://git.framasoft.org/groolot/liveUSB/issues>`_
#. Accédez à la `page de création <https://git.framasoft.org/groolot/liveUSB/issues/new?issue>`_ d'une nouvelle *issue*
#. Renseignez le champ ``Title`` d'une phrase très courte décrivant votre proposition : *"changer l'image d'arrière plan du menu de démarrage"*
#. Renseignez le champ ``Description`` d'une description détaillée décrivant votre proposition, avec les éléments minimum suivants :

   * le modèle de votre machine
   * la quantité de mémoire vive (``RAM`` exprimée en ``Go``)
   * le modèle du CPU (ex : ``Dual Core i5-2450M@2.4GHz``)
   * la version de |project| touchée par cette proposition
   * l'ébauche ou le contenu réel de la proposition (croquis, images, *etc.*)
   * où placeriez-vous cette ressource ? (optionnel)

#. Renseigner le champ ``Labels`` à ``tuning``
#. Valider la création de cette nouvelle *issue* en appuyant sur le bouton ``Submit issue``

.. warning::
   **NE PAS RENSEIGNER** les champs ``Assignee`` et ``Milestone``
