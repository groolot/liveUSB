Installation de ``live-build`` pour les utilisateurs **sans** Debian GNU/Linux
##############################################################################
Récupération du code source
===========================
``live-build`` est développé en utilisant le système de contrôle de version Git. Dans les
systèmes basés sur Debian, il est fourni par le paquet ``git``. Pour examiner le dernier
code, exécutez :

.. highlight:: none
::

   $ git clone https://anonscm.debian.org/cgit/debian-live/live-build.git


Installation de ``live-build``
==============================
Vous pouvez également installer ``live-build`` directement sur votre système en exécutant :

.. highlight:: none
::

   # make install


et le désinstaller avec :

.. highlight:: none
::

   # make uninstall

