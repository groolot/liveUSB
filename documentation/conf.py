#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# liveUSB documentation build configuration file
#
# This file is execfile()d with the current directory set to its
# containing dir.
#

import sys
import os
import shutil

if os.environ.get('READTHEDOCS', None) == 'True':
    import shutil
    shutil.rmtree('../config',True)
    shutil.rmtree('../auto',True)


# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#sys.path.insert(0, os.path.abspath('.'))

# -- General configuration ------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
needs_sphinx = '1.3'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['sphinx.ext.extlinks']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['.templates']
source_suffix = '.rst'

master_doc = 'index'
user_doc = 'utilisateur'
contrib_doc = 'contributeur'
distrib_doc = 'distributeur'
construct_doc = 'constructeur'

distribution_names = {
    'DISTRIBUTION': 'DISTRIBUTION',
    'jessie': 'stable',
    'stretch': 'testing',
    'sid': 'unstable',
}
project = 'liveUSB'
author = 'Gregory DAVID <gregory.david@ac-nantes.fr>'
copyright = '2016, ' + author
architecture = 'amd64'
distribution = 'jessie'
areas = 'main contrib non-free'

version = '0.1'
release = '0.1.5-jessie'

extlinks = {
    'issue': ('https://git.framasoft.org/groolot/liveUSB/issues/%s','issue #'),
    'milestone': ('https://git.framasoft.org/groolot/liveUSB/milestones/%s','milestone '),
    'file': ('https://git.framasoft.org/groolot/liveUSB/blob/' + distribution_names[distribution] + '/%s','le fichier '),
    'merge': ('https://git.framasoft.org/groolot/liveUSB/merge_requests/%s','merge request !'),
    'snippet': ('https://git.framasoft.org/groolot/liveUSB/snippets/%s','bout de code '),
    'commit': ('https://git.framasoft.org/groolot/liveUSB/commit/%s','commit '),
    'tag': ('https://git.framasoft.org/groolot/liveUSB/tags/%s','tag '),
    'img': ('http://liveusb.groolot.net/' + distribution_names[distribution] + '/liveUSB-v' + version + '-' + architecture + '%s','disponible au format '),
}

language = 'fr'
today_fmt = '%d/%m/%Y'
exclude_patterns = ['.build']
show_authors = True
pygments_style = 'sphinx'
todo_include_todos = False

# A string of reStructuredText that will be included at the beginning of every source file that is read.
rst_prolog = """
.. |architecture| replace:: ``""" + architecture + """``
.. |distribution| replace:: ``""" + distribution + """``
.. |areas| replace:: ``""" + areas + """``
.. |project| replace:: ``""" + project + """``
.. |author| replace:: """ + author + """
"""


# -- Options for HTML output ----------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = 'default'

#html_logo = None
#html_favicon = None
html_static_path = ['.static']
html_use_index = False
html_search_language = 'fr'
htmlhelp_basename = 'liveUSBdoc'

# -- Options for LaTeX output ---------------------------------------------

latex_elements = {
    'papersize': 'a4paper',
}
latex_documents = [
    (master_doc, project + '-' + release + '.tex', 'Documentation globale de ' + project, author, 'manual'),
    (user_doc, user_doc + '-' + project + '-' + release + '.tex', '', author, 'manual'),
    (contrib_doc, contrib_doc + '-' + project + '-' + release + '.tex', '', author, 'manual'),
    (distrib_doc, distrib_doc + '-' + project + '-' + release + '.tex', '', author, 'manual'),
    (construct_doc, construct_doc + '-' + project + '-' + release + '.tex', '', author, 'manual'),
]

#latex_logo = None
#latex_use_parts = False
latex_show_pagerefs = True
latex_show_urls = 'footnote'
latex_appendices = [
    'nomenclature',
    'prerequis',
    'credits',
]

latex_domain_indices = False
