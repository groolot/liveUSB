Documentation distributeur
##########################
Téléchargement de l'image système
=================================

La dernière image système validée et construite de |project| est :img:`.img`.

Vous pouvez consulter les autres constructions en visitant `liveUSB <http://liveusb.groolot.net>`_.

.. _transfert+persistance:

Transfert de l'image système et configuration de la persistance
==================================================================

Choisir une méthode parmi les :ref:`methodeA` (automatisée) ou  :ref:`methodeB` (étape par étape).

.. note::
   Ces méthodes utilisent exclusivement des outils **libres** GNU/Linux.

.. warning::
   Pour les utilisateurs n'ayant pas un accès direct à GNU/Linux, une solution est possible la :ref:`methodeC`.

Le mécanisme général est le suivant :
 * copie de l'image sur le périphérique USB (utilisation de ``dd``)
 * création de la partition sur l'espace vide restant sur le périphérique USB (``gparted``, ``sfdisk``, etc.)
 * formatage de la partition en ``ext4`` (``mkfs.ext4``)
 * configuration de la persistance dans le fichier ``persistence.conf``

Nomenclature
------------

Voir :doc:`nomenclature` pour des explications sur la nomenclature concernant les lignes de commande.

.. _methodeA:

[Méthode A - rapide, simple et préférée] Sous GNU/Linux, méthode automatisée
----------------------------------------------------------------

Cette méthode est conçue pour faciliter le travail. L'automate ``burn`` peut-être utilisé.

Dans notre cas, la **configuration de base promet d'assurer la persistance à partir d'une
partition utilisant l'espace restant libre sur le support cible**.

.. note::
   Vous pouvez analyser :file:`le fichier burn <burn#883>` de la ligne 883 à la fin afin de
   comprendre la façon dont cette étape est assurée.

Considérons que votre clef USB est associé au descripteur ``/dev/sde`` et que le fichier image système est dans ``liveUSB-amd64.img`` :

.. highlight:: none
::

   # ./burn --image liveUSB-amd64.img --device /dev/sde

ou
   
.. highlight:: none
::

   $ sudo ./burn --image liveUSB-amd64.img --device /dev/sde


.. note::
   Aide en ligne de ``burn``
   
   .. highlight:: none
   ::

      $ sudo ./burn --help
      burn v0.1.3-jessie
      Brûler l'image de la clef liveUSB sur un périphérique choisi et configurer l'espace restant disponible pour accueillir la persistance du système.
      
      Usage : burn [-h | --help] [[-d | --device] chemin] [[-i | --image] fichier.img] [-C | --no-copy] [-P | --no-persistence] [-K | --no-crypt] [-E | --no-efi | --no-uefi] [-v | --verbose | --debug] [-V | --very-verbose] [-u | --usage]
      
      Options :

        SYSTÈME :
	-i, --image=fichier.img
	                Image de la clef à brûler
	-d, --device=chemin
	                Bloc device (périphérique mode bloc) destinataire
	-c, --copy      Force l'étape de copie de l'image (défaut)
	-C, --no-copy   Désactive l'étape de copie de l'image

	-p, --persistence
	                Force l'étape de persistance (défaut)
	-P, --no-persistence
	                Désactive l'étape de persistance

	CHIFFREMENT DE LA PERSISTANCE :
	-k, --crypt     Force le chiffrement de la partition de persistance (défaut)
	-K, --no-crypt  Désactive le chiffrement de la partition de persistance

	EFI / APPLE MAC :
	-e, --efi, --uefi
	                Force le démarrage en UEFI (défaut)
	-E, --no-efi, --no-uefi
	                Désactive le démarrage en UEFI (ne démarrera pas sur un Apple Mac)

	GÉNÉRALES :
	-h, --help      Affiche ce message d'aide et quitte
	--color         Force l'usage des couleurs ANSI
	--no-color      Désactive l'usage des couleurs ANSI (défaut)
	-v, --verbose, --debug
	                Affiche des informations supplémentaires
	-V, --very-verbose
	                Affiche le DEBUGAGE du script (set -x)
	--silent, --quiet
	                Tente d'être le plus discret possible
	                (seule les erreurs seront affichées en plus des intéractions utilisateur)

	NOTE : Vous devez avoir les droits 'sudo' ou être le super utilisateur pour exécuter ce programme.

.. _methodeB:

[Méthode B - pour comprendre] Sous GNU/Linux, méthode étape par étape
---------------------------------------------------------------------

Il s'agit de la même démarche que la :ref:`methodeA`, mais il s'agit ici d'une version non automatisée.

Cette  :ref:`methodeB` a été testée dans l':issue:`36` (on y trouvera quelques erreurs, et les explications concernant leurs résolutions).

Identification du périphérique cible
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Avant de brancher la clef USB, lancer la commande ``lsblk`` :

.. highlight:: none
::

   $ lsblk
   NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
   sda      8:0    0 465,8G  0 disk 
   +-sda1   8:1    0 465,8G  0 part /
   sdb      8:16   0   1,8T  0 disk 
   +-sdb1   8:17   0   1,8T  0 part /home


Brancher maintenant la clef USB, puis relancer ``lsblk`` :

.. highlight:: none
::

   $ lsblk
   NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
   sda      8:0    0 465,8G  0 disk 
   +-sda1   8:1    0 465,8G  0 part /
   sdb      8:16   0   1,8T  0 disk 
   +-sdb1   8:17   0   1,8T  0 part /home
   sdc      8:64   1  14,5G  0 disk

**Nous pouvons alors identifier que notre clef USB est accessible par le descripteur en mode bloc** ``/dev/sdc``.

Nous pouvons vérifier que c'est un périphérique en mode bloc par le ``b`` en début de ligne de sortie de la commande suivante :

.. highlight:: none
::

   $ ls -l /dev/sdc
   brw-rw---- 1 root disk 8, 32 févr. 15 19:03 /dev/sdc


Recopie de l'image système sur le périphérique cible (= la clef USB)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Vous devez vous munir du nom du descripteur en mode bloc du périphérique USB cible. Dans cet exemple, nous considérerons le périphérique USB accessible sur ``/dev/sdc``.

Vous devez par ailleurs vous munir du nom de l'image système précédemment créée. Si vous avez laissé la configuration initiale, le fichier image se nomme ``liveUSB-amd64.img``. Placez-vous dans le dossier contenant ce fichier image, puis exécutez la commande ci-desssous. Il faut attendre quelques minutes avant que l'écran affiche que la commande s'est bien exécutée.

.. highlight:: none
::

   # dd if=liveUSB-amd64.img of=/dev/sdc bs=1M
   1634+0 enregistrements lus
   1634+0 enregistrements écrits
   1713373184 bytes (1,7 GB, 1,6 GiB) copied, 167,139 s, 10,3 MB/s

.. _persistanceconcept:
   
Mise en œuvre de la persistance (voir le `live-manual <http://debian-live.alioth.debian.org/live-manual/unstable/manual/html/live-manual.en.html#558>`_)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Il s'agit ici de comprendre les concepts. **Il n'y a pas de démarche technique à réaliser**. Si vous le souhaitez, passez directement à l'étape :ref:`persistancepartition`.

  Le paradigme d'un système *Live CD* est d’être pré-installé en s'amorçant (*boot*) sur un support
  en lecture seule, comme un cdrom, où les données et les modifications ne survivent
  pas aux redémarrages du matériel hôte qui l’exécute.

  Un système *live* est une généralisation de ce paradigme et gère ainsi d’autres supports
  en plus des CDs. Malgré tout, dans son comportement par défaut, il doit être considéré
  en lecture seule et toutes les évolutions pendant l’exécution du système sont perdues
  à l’arrêt.

  La **persistance** est un nom commun pour les différents types de solutions pour
  sauver, après un redémarrage, certaines ou toutes les données, de cette évolution
  pendant l’exécution du système. Pour comprendre comment cela fonctionne, il peut être
  utile de savoir que même si le système est démarré et exécuté à partir d’un support en
  lecture seule, les modifications des fichiers et répertoires sont écrites sur des supports
  inscriptibles, typiquement un disque RAM (``tmpfs``) et les données des disques RAM ne
  survivent pas à un redémarrage.

  Les données stockées sur ce disque virtuel doivent être enregistrées sur un support
  inscriptible persistant comme un support de stockage local, un partage réseau. Tous
  ces supports sont pris en charge dans les systèmes *live* de différentes manières,
  et tous nécessitent un paramètre d’amorçage spécial à préciser au moment du démarrage :
  ``persistence``.

  Si le paramètre de démarrage ``persistence`` est réglé (et ``nopersistence`` n’est pas
  utilisé), les supports de stockage locaux (par exemple les disques durs, clés USB)
  seront examinés pour trouver des volumes persistants pendant le démarrage. Il est
  possible de limiter les types de volumes persistants à utiliser en indiquant certains
  paramètres de démarrage décrits dans la page de manuel ``live-boot(7)``. Un volume
  persistant est un des éléments suivants :
   * une partition, identifiée par son nom ``GPT`` ou ``msdos``,
   * **un système de fichiers, identifié par son étiquette (son nom) de système de fichiers**,
   * un fichier image situé sur la racine d’un système de fichiers en lecture (même une partition NTFS d’un système d’exploitation étranger), identifié par son nom de fichier.

.. _persistancepartition:

Création et paramétrage de la partition d'accueil pour la persistance
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Vous devez vous munir du nom du descripteur en mode bloc du périphérique USB cible. Dans cet exemple, nous considérerons le périphérique USB accessible sur ``/dev/sdc``, tel que celui utilisé pour réalisé le transfert de l'image système.

.. highlight:: none
::

   # echo ",+,L" | sfdisk --append /dev/sdc

Concrètement, on peut avoir besoin d'écrire ``sudo`` avant et après ``|``, sinon cela ne fonctionne pas car il nous manque des droits. De plus, le paramètre ``--force`` peut être indispensable. Ainsi, l'instruction s'écrit ainsi ``$ sudo echo ",+,L" | sudo sfdisk --append --force /dev/sdc``

Il y a ensuite deux instructions à saisir : la première informe le système de la mise à jour de la table de partition de la clé, tandis que la seconde formate et nomme la partition de persistance.


.. highlight:: none
::

   # partprobe /dev/sdc
   # mkfs.ext4 -F -L persistence /dev/sdc

Ensuite, il faut aller dans la partition de persistance ``persistence``, créer le fichier ``persistence.conf``, dont le contenu est :
::

   / union

Voir :issue:`89#note_14328` et :issue:`36#note_11929` pour des essais réussis concernant cette étape de création de la partition d'accueil pour la persistance.

.. _methodeC:

[Méthode C] Sous Windows
------------------------


Nature du problème
^^^^^^^^^^^^^^^^^^
Les recherches réalisés dans l':issue:`89` montrent que Windows ne comporte pas d'outil intégré pour faire la distribution de notre clé |project|. Et la recherche d'un logiciel (libre) pour répondre à ce besoin n'a rien donné.

Il y a deux raisons à cela :
 - **Lorsqu'une clef USB comporte plusieurs partitions, l'explorateur Windows n'est capable d'afficher que la première partition**. Nous voulons justement utiliser une première partition pour le système, et une deuxième partition pour les données de persistance.
 - **Windows ne gère pas le format de fichier** ``ext4``. Or c'est justement ce format qui est adapté pour la persistance. En effet, le format de fichiers souvent utilisé pour les clefs USB est le ``FAT32``. Mais ce format présente une limitation de taille de fichier de presque 4 Gio (2\ :sup:`32` octets pour être exact).

Nous pouvons adopter deux positions :
 - Soit on abandonne l'idée d'avoir deux partitions séparées et on abandonne l'idée d'utiliser ``ext4``. Cela signifie rester en ``FAT32``, et mettre la persistance dans un fichier dédié (mais sa taille sera limitée à presque 4 Gio). C'est une solution possible, qui est d'ailleurs utilisée par beaucoup de clefs démarrables.
 - Soit on **abandonne l'idée de construire la clé** |project| **en restant sous Windows uniquement**. C'est la démarche que nous proposons.

Solution proposée : création d'une clef démarrable GNU/Linux (qui servira de boîte à outils intermédiaire)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Nous proposons une solution avec ``Lubuntu``, mais d'autres distributions basées sur Debian pourraient convenir (mais nous n'avons pas eu le temps d'en tester). On pourra consulter l':issue:`89` pour comprendre les étapes qui nous amènent à cette solution.

- On télécharge directement l'iso ``lubuntu-15.10-desktop-i386.iso`` sur http://lubuntu.fr/down.htm (sans passer par LinuxLive USB Creator).
.. attention::
   Les versions plus anciennes de ``lubuntu`` ne conviennent pas !
   
- Avec `LinuxLive USB Creator <http://www.linuxliveusb.com/>`_, on sélectionne cette ``iso``. Même si cette ``iso`` n'est pas dans la liste de compatibilité, LinuxLive USB Creator accepte quand même de créer un fichier de persistance (c'est ici un fichier dédié pour la persistance, il a une taille limitée à presque 4 Gio = 2\ :sup:`32` octets exactement).
- La persistance n'est pas indispensable ici. On pourrait s'en passer, mais on en profite car cela pourrait servir dans d'autres situations.
- Après gravure de la clef, redémarrer cette clef, et ensuite suivre la :ref:`methodeA` ou la :ref:`methodeB`.

.. figure:: screenshot-linux-live-usb-creator.png
   :alt: Interface de Linux Live USB Creator

   Interface de Linux Live USB Creator

Et si on dispose déjà d'une clef liveUSB ?
==========================================


On peut l'utiliser pour en fabriquer une autre, il suffit de démarrer sur la première clef et de suivre la documentation depuis :ref:`transfert+persistance`.
