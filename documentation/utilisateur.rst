Documentation utilisateur
#########################


Comment permettre le démarrage du système sur la clé
====================================================

Utiliser le menu de démarrage
-----------------------------
Au démarrage, il faut appuyer sur la touche qui indique à l'ordinateur de démarrer sur la clé, et non pas sur le disque dur. Pour trouver cette touche, cela dépend de votre matériel et vous devrez probablement être attentif aux informations affichées lors de l'allumage de l'ordinateur.

.. note::

   Exemples : :kbd:`F8` (PC de bureau) ou :kbd:`F12` (PC portable).

Les périphériques suivants s'affichent :
 * ``USB Mass Storage Device``
 * ``UEFI: SanDisk`` <= celui-ci
 * ``SanDisk`` <= ou celui-là

Entrer dans le BIOS
-------------------
Si l'étape précédente n'a pas fonctionné, c'est qu'elle n'a pas été activée, ou qu'elle n'est pas disponible. Il faut alors aller dans le ``BIOS``, et activer le menu de démarrage (*boot menu*), ou bien (directement à partir du ``BIOS``)  démarrer sur la clé. Exemples de touches à utiliser au démarrage : :kbd:`DEL` ou :kbd:`F2`.

Configuration par défaut
========================
Le nom du compte d'utilisateur (*login*) est ``utilisateur``, c'est celui qui apparaîtra dans le terminal.

Le *nom visible* est ``Utilisateur liveUSB``, c'est ce qui apparaît dans Gnome3 ou d'autres applications.

.. note::
   
   Le mot de passe du compte ``utilisateur`` est ``live``.
	
Comment installer un paquet manquant
====================================
Par exemple, ``python-pygame`` n'est pas installé par défaut. Pour l'installer s'il y a besoin, voir l':issue:`18`.

Erreurs ou avertissements possibles
===================================

Apple Macintosh
---------------

Depuis la version ``v0.1.4``, les images construites sont compatibles avec les Apple Macintosh qui démarrent en ``UEFI``.

Windows is hibernated
---------------------
Ce message peut apparaître au démarrage de la clé :

.. highlight:: none
::

  ntfs: (device sda2) : load_system_files(): Windows is hibernated. Mounting read-only. Run chkdsk

Cela provient du fait que Windows n'est pas vraiment éteint par défaut (Windows 8, Windows 10 par exemple). Il est en hibernation, afin de redémarrer plus vite. Il n'y a pas de problème lié à la clé *live*.

S'il y a besoin de monter la partition Windows, alors il faut éteindre complètement Windows avant de lancer la clé *live*.

Voir `le forum de discussion <http://askubuntu.com/questions/145902/unable-to-mount-windows-ntfs-filesystem-due-to-hibernation>`_.

Undefined video mode number: 317
--------------------------------

.. highlight:: none
::
   
   Undefined video mode number: 317
   Press <ENTER> to see video modes available, <SPACE> to continue, or wait 30 sec

L'écran du ``Asus Eee PC 1015PEM`` a une résolution de ``1024x600 pixels``, ce mode semble provoquer ce message (j'ai essayé en choisissant :kbd:`ENTER` mais la résolution ``1024x600`` n'est pas proposée).

Donc ici on appuie directement sur :kbd:`SPACE` et tout se déroule correctement ensuite.

Le système semble gelé en sortie de veille
------------------------------------------
Il est arrivé, sur un système de test, que la sortie de veille du système *live* gèle la machine. Ceci a été discuté dans l':issue:`16` et ne semble pas avoir été rencontré à nouveau. Si toutefois vous rencontrez ce dysfonctionnement et qu'il est reproductible (c'est à dire si vous pouvez le refaire plusieurs fois selon la même méthode), venez alimenter l':issue:`16` avec les informations suivantes :

* modèle de votre machine,
* quantité de mémoire vive (``RAM`` exprimée en ``Go``),
* modèle du CPU (ex : ``Dual Core i5-2450M@2.4GHz``),
* version de |project| touchée par le dysfonctionnement,
* **description** de la démarche pour reproduire le dysfonctionnement.
