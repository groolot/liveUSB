.. _prerequis:

Pré-requis
----------
Les exigences pour la création ou la distribution de l'images système |project| sont faibles :
 * Accès super-utilisateur (``root``) sur le système
 * Un shell ``bash >=4.3``
 * Un noyau ``Linux >=2.6``
 * Une version mise à jour de ``live-build >=4.0.3`` (voir :doc:`nondebian`)
 * Une version à jour de ``GNU make >=3.81``
 * Une version à jour de ``cryptsetup >=1.6.6``
 * Une version à jour de ``dd >=8.23`` (fourni par le paquet ``coreutils``)
 * Une version à jour de ``debootstrap >=1.0.67``
 * Une version à jour de ``git >=2.1.4``
 * Une version à jour de ``losetup >=2.26`` (fourni par le paquet ``util-linux``)
 * Une version à jour de ``partprobe >=3.2`` (fourni par le paquet ``parted``)
 * Une version à jour de ``pv >=1.5.7``
 * Une version à jour de ``sfdisk >=2.26``
 * Une version à jour de ``syslinux >=6.03`` fournissant l'EFI (``syslinux-efi >=6.03`` pour Debian)
 * Une version à jour de ``whiptail >=0.52.17`` ou ``dialog >=1.2``
