
Projet ``liveUSB v0.1.5-jessie``
==============================

.. image:: documentation/presentations/data/screenshot-desktop.png

**Clef USB démarrable**, support de l'**enseignement de l'informatique** (peut être construite pour d'autres usages : militantisme, sécurité, sauvegarde, *etc.*).

Contient une version adaptée de **Debian GNU/Linux**.

C'est une clef **persistante** (les modifications sont conservées entre les redémarrages) et **sécurisée** (la partition de persistance est chiffrée avec `LUKS - Linux Unified Key Setup <https://fr.wikipedia.org/wiki/LUKS>`_).

Documentation
=============

.. image:: http://readthedocs.org/projects/liveusb/badge/?version=master
   :target: http://liveusb.readthedocs.org/fr/master/?badge=master
   :alt: État de la documentation

La documentation est disponible `en ligne (dernière version) <http://liveusb.groolot.net/master/documentation/html/>`_ ou `en PDF (toutes les versions) <http://liveusb.groolot.net/?dir=master/documentation>`_.


