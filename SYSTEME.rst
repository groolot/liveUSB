
Configuration v0.1.5-jessie
=====================
Architecture matérielle supportée
---------------------------------
``amd64``

Distribution Debian GNU/Linux
-----------------------------
Version ``jessie``

Mirroir dépôt de téléchargement des paquets
-------------------------------------------
http://ftp.debian.org/debian/

Étendues des zones d'accès des paquets
--------------------------------------
Les paquets installables peuvent être issus des zones suivantes : ``main contrib non-free``

Liste des paquets installés
----

Représentation de l'information
^^^^

Représentation binaire
"""""

* `bless <https://packages.debian.org/jessie/bless>`_
* `hexedit <https://packages.debian.org/jessie/hexedit>`_
* `shed <https://packages.debian.org/jessie/shed>`_
* `wxhexeditor <https://packages.debian.org/jessie/wxhexeditor>`_

Opérations booléennes
"""""

* `logisim <https://packages.debian.org/jessie/logisim>`_

Numérisation
"""""

* `gimp <https://packages.debian.org/jessie/gimp>`_
* `gimp-data-extras <https://packages.debian.org/jessie/gimp-data-extras>`_
* `gimp-plugin-registry <https://packages.debian.org/jessie/gimp-plugin-registry>`_
* `gimp-texturize <https://packages.debian.org/jessie/gimp-texturize>`_
* `inkscape <https://packages.debian.org/jessie/inkscape>`_
* `xsane <https://packages.debian.org/jessie/xsane>`_

Formats de données
"""""

* `bless <https://packages.debian.org/jessie/bless>`_
* `fontforge <https://packages.debian.org/jessie/fontforge>`_
* `gdb <https://packages.debian.org/jessie/gdb>`_
* `hexedit <https://packages.debian.org/jessie/hexedit>`_
* `python3 <https://packages.debian.org/jessie/python3>`_
* `shed <https://packages.debian.org/jessie/shed>`_
* `wxhexeditor <https://packages.debian.org/jessie/wxhexeditor>`_

Compression
"""""

* `file-roller <https://packages.debian.org/jessie/file-roller>`_
* `unar <https://packages.debian.org/jessie/unar>`_
* `unrar-free <https://packages.debian.org/jessie/unrar-free>`_
* `unzip <https://packages.debian.org/jessie/unzip>`_
* `zip <https://packages.debian.org/jessie/zip>`_

Structuration et organisation de l'information
"""""

* `gparted <https://packages.debian.org/jessie/gparted>`_
* `lxterminal <https://packages.debian.org/jessie/lxterminal>`_
* `nautilus <https://packages.debian.org/jessie/nautilus>`_
* `xterm <https://packages.debian.org/jessie/xterm>`_

Algorithmique
^^^^

* `algobox <https://packages.debian.org/jessie/algobox>`_
* `logisim <https://packages.debian.org/jessie/logisim>`_

Langages et programmation
^^^^

* `arduino <https://packages.debian.org/jessie/arduino>`_
* `emacs <https://packages.debian.org/jessie/emacs>`_
* `geany <https://packages.debian.org/jessie/geany>`_
* `geany-plugin-py <https://packages.debian.org/jessie/geany-plugin-py>`_
* `gedit <https://packages.debian.org/jessie/gedit>`_
* `graphviz <https://packages.debian.org/jessie/graphviz>`_
* `ipython3 <https://packages.debian.org/jessie/ipython3>`_
* `ipython3-notebook <https://packages.debian.org/jessie/ipython3-notebook>`_
* `ipython3-qtconsole <https://packages.debian.org/jessie/ipython3-qtconsole>`_
* `pd-boids <https://packages.debian.org/jessie/pd-boids>`_
* `pd-chaos <https://packages.debian.org/jessie/pd-chaos>`_
* `pd-fftease <https://packages.debian.org/jessie/pd-fftease>`_
* `pd-freeverb <https://packages.debian.org/jessie/pd-freeverb>`_
* `pd-ggee <https://packages.debian.org/jessie/pd-ggee>`_
* `pd-hcs <https://packages.debian.org/jessie/pd-hcs>`_
* `pd-hid <https://packages.debian.org/jessie/pd-hid>`_
* `pd-iemlib <https://packages.debian.org/jessie/pd-iemlib>`_
* `pd-iemnet <https://packages.debian.org/jessie/pd-iemnet>`_
* `pd-libdir <https://packages.debian.org/jessie/pd-libdir>`_
* `pd-osc <https://packages.debian.org/jessie/pd-osc>`_
* `pd-zexy <https://packages.debian.org/jessie/pd-zexy>`_
* `puredata <https://packages.debian.org/jessie/puredata>`_
* `puredata-extra <https://packages.debian.org/jessie/puredata-extra>`_
* `puredata-import <https://packages.debian.org/jessie/puredata-import>`_
* `puredata-utils <https://packages.debian.org/jessie/puredata-utils>`_
* `python-pyo <https://packages.debian.org/jessie/python-pyo>`_
* `python3-doc <https://packages.debian.org/jessie/python3-doc>`_
* `python3-kivy <https://packages.debian.org/jessie/python3-kivy>`_
* `python3-liblo <https://packages.debian.org/jessie/python3-liblo>`_
* `python3-matplotlib <https://packages.debian.org/jessie/python3-matplotlib>`_
* `python3-numpy <https://packages.debian.org/jessie/python3-numpy>`_
* `python3-pil <https://packages.debian.org/jessie/python3-pil>`_
* `python3-pil.imagetk <https://packages.debian.org/jessie/python3-pil.imagetk>`_
* `python3-pip <https://packages.debian.org/jessie/python3-pip>`_
* `python3-scipy <https://packages.debian.org/jessie/python3-scipy>`_
* `python3-tk <https://packages.debian.org/jessie/python3-tk>`_
* `scratch <https://packages.debian.org/jessie/scratch>`_
* `spyder3 <https://packages.debian.org/jessie/spyder3>`_
* `zenity <https://packages.debian.org/jessie/zenity>`_

Correction d'un programme
"""""

* `alleyoop <https://packages.debian.org/jessie/alleyoop>`_
* `cppcheck <https://packages.debian.org/jessie/cppcheck>`_
* `doxygen <https://packages.debian.org/jessie/doxygen>`_
* `doxygen-gui <https://packages.debian.org/jessie/doxygen-gui>`_
* `gdb <https://packages.debian.org/jessie/gdb>`_
* `valgrind <https://packages.debian.org/jessie/valgrind>`_
* `valkyrie <https://packages.debian.org/jessie/valkyrie>`_

Langages de description
"""""

* `auctex <https://packages.debian.org/jessie/auctex>`_
* `bluefish <https://packages.debian.org/jessie/bluefish>`_
* `gedit <https://packages.debian.org/jessie/gedit>`_
* `texlive <https://packages.debian.org/jessie/texlive>`_
* `texlive-bibtex-extra <https://packages.debian.org/jessie/texlive-bibtex-extra>`_
* `texlive-lang-french <https://packages.debian.org/jessie/texlive-lang-french>`_
* `texlive-latex-extra <https://packages.debian.org/jessie/texlive-latex-extra>`_
* `texlive-latex-recommended <https://packages.debian.org/jessie/texlive-latex-recommended>`_
* `texmaker <https://packages.debian.org/jessie/texmaker>`_

Architectures matérielles
^^^^

* `binutils <https://packages.debian.org/jessie/binutils>`_
* `build-essential <https://packages.debian.org/jessie/build-essential>`_
* `gdb <https://packages.debian.org/jessie/gdb>`_
* `pciutils <https://packages.debian.org/jessie/pciutils>`_

Réseaux
^^^^

* `arping <https://packages.debian.org/jessie/arping>`_
* `filezilla <https://packages.debian.org/jessie/filezilla>`_
* `netcat <https://packages.debian.org/jessie/netcat>`_
* `openssh-blacklist <https://packages.debian.org/jessie/openssh-blacklist>`_
* `openssh-blacklist-extra <https://packages.debian.org/jessie/openssh-blacklist-extra>`_
* `openssh-client <https://packages.debian.org/jessie/openssh-client>`_
* `openssh-server <https://packages.debian.org/jessie/openssh-server>`_
* `openssh-sftp-server <https://packages.debian.org/jessie/openssh-sftp-server>`_
* `openssl <https://packages.debian.org/jessie/openssl>`_
* `pd-iemnet <https://packages.debian.org/jessie/pd-iemnet>`_
* `pd-osc <https://packages.debian.org/jessie/pd-osc>`_
* `python-scapy <https://packages.debian.org/jessie/python-scapy>`_
* `python3-liblo <https://packages.debian.org/jessie/python3-liblo>`_
* `seahorse <https://packages.debian.org/jessie/seahorse>`_
* `wireshark <https://packages.debian.org/jessie/wireshark>`_

Mathématiques
^^^^

* `geogebra <https://packages.debian.org/jessie/geogebra>`_
* `maxima <https://packages.debian.org/jessie/maxima>`_
* `wxmaxima <https://packages.debian.org/jessie/wxmaxima>`_
* `xmaxima <https://packages.debian.org/jessie/xmaxima>`_

Global
^^^^

Jeux
"""""


Visualiseurs
"""""

* `evince <https://packages.debian.org/jessie/evince>`_
* `gedit <https://packages.debian.org/jessie/gedit>`_
* `impressive <https://packages.debian.org/jessie/impressive>`_
* `libreoffice-presenter-console <https://packages.debian.org/jessie/libreoffice-presenter-console>`_
* `mplayer <https://packages.debian.org/jessie/mplayer>`_
* `pdf-presenter-console <https://packages.debian.org/jessie/pdf-presenter-console>`_
* `vlc <https://packages.debian.org/jessie/vlc>`_
* `xpdf <https://packages.debian.org/jessie/xpdf>`_

Son
"""""

* `audacity <https://packages.debian.org/jessie/audacity>`_
* `libav-tools <https://packages.debian.org/jessie/libav-tools>`_
* `sox <https://packages.debian.org/jessie/sox>`_

Image
"""""

* `blender <https://packages.debian.org/jessie/blender>`_
* `fyre <https://packages.debian.org/jessie/fyre>`_
* `gimp <https://packages.debian.org/jessie/gimp>`_
* `imagemagick <https://packages.debian.org/jessie/imagemagick>`_

Vidéo
"""""

* `libav-tools <https://packages.debian.org/jessie/libav-tools>`_
* `mplayer <https://packages.debian.org/jessie/mplayer>`_
* `vlc <https://packages.debian.org/jessie/vlc>`_

Systèmes de gestion de version (SCM)
"""""

* `git <https://packages.debian.org/jessie/git>`_
* `git-doc <https://packages.debian.org/jessie/git-doc>`_
* `git-gui <https://packages.debian.org/jessie/git-gui>`_
* `gitk <https://packages.debian.org/jessie/gitk>`_
* `meld <https://packages.debian.org/jessie/meld>`_

Virtualisation
"""""

* `qemu-kvm <https://packages.debian.org/jessie/qemu-kvm>`_
* `qemu-utils <https://packages.debian.org/jessie/qemu-utils>`_
* `virt-manager <https://packages.debian.org/jessie/virt-manager>`_
* `virt-viewer <https://packages.debian.org/jessie/virt-viewer>`_
* `virtualbox <https://packages.debian.org/jessie/virtualbox>`_
* `virtualbox-qt <https://packages.debian.org/jessie/virtualbox-qt>`_

Navigateurs web
"""""

* `chromium <https://packages.debian.org/jessie/chromium>`_
* `flashplugin-nonfree <https://packages.debian.org/jessie/flashplugin-nonfree>`_
* `iceweasel <https://packages.debian.org/jessie/iceweasel>`_
* `iceweasel-l10n-fr <https://packages.debian.org/jessie/iceweasel-l10n-fr>`_
* `xul-ext-adblock-plus <https://packages.debian.org/jessie/xul-ext-adblock-plus>`_
* `xul-ext-flashblock <https://packages.debian.org/jessie/xul-ext-flashblock>`_

Bureautique
"""""

* `freeplane <https://packages.debian.org/jessie/freeplane>`_
* `gedit <https://packages.debian.org/jessie/gedit>`_
* `libreoffice <https://packages.debian.org/jessie/libreoffice>`_
* `libreoffice-l10n-fr <https://packages.debian.org/jessie/libreoffice-l10n-fr>`_
* `libreoffice-presenter-console <https://packages.debian.org/jessie/libreoffice-presenter-console>`_
* `hunspell <https://packages.debian.org/jessie/hunspell>`_
* `hunspell-fr-modern <https://packages.debian.org/jessie/hunspell-fr-modern>`_

Communication
"""""

* `ekiga <https://packages.debian.org/jessie/ekiga>`_
* `hexchat <https://packages.debian.org/jessie/hexchat>`_
* `mumble <https://packages.debian.org/jessie/mumble>`_
* `pidgin <https://packages.debian.org/jessie/pidgin>`_

Système et outils de base
^^^^

* `avahi-daemon <https://packages.debian.org/jessie/avahi-daemon>`_
* `binutils <https://packages.debian.org/jessie/binutils>`_
* `broadcom-sta-dkms <https://packages.debian.org/jessie/broadcom-sta-dkms>`_
* `build-essential <https://packages.debian.org/jessie/build-essential>`_
* `console-data <https://packages.debian.org/jessie/console-data>`_
* `console-setup <https://packages.debian.org/jessie/console-setup>`_
* `cryptsetup <https://packages.debian.org/jessie/cryptsetup>`_
* `cups <https://packages.debian.org/jessie/cups>`_
* `cups-filters <https://packages.debian.org/jessie/cups-filters>`_
* `cups-pk-helper <https://packages.debian.org/jessie/cups-pk-helper>`_
* `debian-installer-launcher <https://packages.debian.org/jessie/debian-installer-launcher>`_
* `ekiga <https://packages.debian.org/jessie/ekiga>`_
* `evince <https://packages.debian.org/jessie/evince>`_
* `file <https://packages.debian.org/jessie/file>`_
* `file-roller <https://packages.debian.org/jessie/file-roller>`_
* `filezilla <https://packages.debian.org/jessie/filezilla>`_
* `firmware-atheros <https://packages.debian.org/jessie/firmware-atheros>`_
* `firmware-brcm80211 <https://packages.debian.org/jessie/firmware-brcm80211>`_
* `firmware-iwlwifi <https://packages.debian.org/jessie/firmware-iwlwifi>`_
* `firmware-zd1211 <https://packages.debian.org/jessie/firmware-zd1211>`_
* `flashplugin-nonfree <https://packages.debian.org/jessie/flashplugin-nonfree>`_
* `foomatic-db-compressed-ppds <https://packages.debian.org/jessie/foomatic-db-compressed-ppds>`_
* `gvfs <https://packages.debian.org/jessie/gvfs>`_
* `gvfs-bin <https://packages.debian.org/jessie/gvfs-bin>`_
* `keyboard-configuration <https://packages.debian.org/jessie/keyboard-configuration>`_
* `less <https://packages.debian.org/jessie/less>`_
* `live-tools <https://packages.debian.org/jessie/live-tools>`_
* `locales <https://packages.debian.org/jessie/locales>`_
* `network-manager-gnome <https://packages.debian.org/jessie/network-manager-gnome>`_
* `printer-driver-cups-pdf <https://packages.debian.org/jessie/printer-driver-cups-pdf>`_
* `printer-driver-gutenprint <https://packages.debian.org/jessie/printer-driver-gutenprint>`_
* `screen <https://packages.debian.org/jessie/screen>`_
* `sudo <https://packages.debian.org/jessie/sudo>`_
* `synaptic <https://packages.debian.org/jessie/synaptic>`_
* `task-french <https://packages.debian.org/jessie/task-french>`_
* `task-french-desktop <https://packages.debian.org/jessie/task-french-desktop>`_
* `task-gnome-desktop <https://packages.debian.org/jessie/task-gnome-desktop>`_
* `unar <https://packages.debian.org/jessie/unar>`_
* `unrar-free <https://packages.debian.org/jessie/unrar-free>`_
* `unzip <https://packages.debian.org/jessie/unzip>`_
* `user-setup <https://packages.debian.org/jessie/user-setup>`_
* `vlc <https://packages.debian.org/jessie/vlc>`_
* `wfrench <https://packages.debian.org/jessie/wfrench>`_
* `xpdf <https://packages.debian.org/jessie/xpdf>`_
* `xul-ext-adblock-plus <https://packages.debian.org/jessie/xul-ext-adblock-plus>`_
* `xul-ext-flashblock <https://packages.debian.org/jessie/xul-ext-flashblock>`_
* `zip <https://packages.debian.org/jessie/zip>`_
* `zsh <https://packages.debian.org/jessie/zsh>`_
