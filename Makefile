CREDENTIALS_DIR = credentials
CREDENTIALS_GPG_FILE = $(CREDENTIALS_DIR)/gpg_signing_key.asc
CREDENTIALS_GPG_FINGERPRINT_FILE = $(CREDENTIALS_DIR)/gpg_current_fingerprint.gpgfp
define CREDENTIALS_GPG_FINGERPRINT =
$(shell cat $(CREDENTIALS_GPG_FINGERPRINT_FILE))
endef
CREDENTIALS_NETRC_FILE = $(CREDENTIALS_DIR)/.netrc
CREDENTIALS_FTP_FILE = $(CREDENTIALS_DIR)/ftp.creds
CREDENTIALS_RSYNC_FILE = $(CREDENTIALS_DIR)/rsync.creds
GPG_SIG_SUFFIX = asc
IMAGE_FILES = $(wildcard liveUSB-v*.img)
DISTRIBUTION_FILES = README.rst SYSTEME.rst burn $(IMAGE_FILES)
DISTRIBUTION_META_FILES = $(wildcard liveUSB-v*.packages) $(wildcard liveUSB-v*.contents) $(wildcard liveUSB-v*.files)
DISTRIBUTION_SIGNATURES_FILES = $(addsuffix .$(GPG_SIG_SUFFIX),$(wildcard liveUSB-v*.img))
DIST_DIR = dist
META_DIR = meta
DOC_DIR=documentation
DOCUMENTATION_HTML_FILE = $(DOC_DIR)/.build/html/index.html
DOCUMENTATION_HTML_DIR = $(DOC_DIR)/.build/html
DOCUMENTATION_REQUISITES = $(wildcard $(DOC_DIR)/*.rst) $(wildcard $(DOC_DIR)/*.png) $(wildcard $(DOC_DIR)/presentations/*.tex) $(wildcard $(DOC_DIR)/presentations/data/*.png)

ifdef SUDO_USER
mySUDO = sudo --user $(SUDO_USER)
endif

define BRANCHE =
$(shell git rev-parse --abbrev-ref HEAD)
endef

.PHONY: all
all: $(BRANCHE)-dist $(BRANCHE)-dist-docs
	@echo "$@'s distribution constructed and placed in $(DIST_DIR)/$*"
	@echo ""
	@echo "You can send all files to FTP server:"
	@echo "      make dist-upload"
	@echo ""
	@echo "or send branch specific upload, for example:"
	@echo "      make unstable-upload"
	@echo ""
	@echo ""
	@echo ""
	@echo ""

.PHONY: help
help:
	@echo "Please use 'make <target>' where <target> is one of"
	@echo "\tbuild\t\tto make the current distribution"
	@echo "\tconfig"
	@echo "\tclean"
	@echo "\tpurge"
	@echo "\tclean-all"
	@echo "\tsanitize\t\tto call a 'git checkout -- .'"
	@echo "\tpdf\t\tto make the documentation in PDF, based on current branch"
	@echo "\thtml\t\tto make the documentation in HTML, based on current branch"
	@echo "\tdocs\t\tto make all documentations (HTML + PDF), based on current branch"
	@echo "\tview-docs\t\tto view resulting documentation (HTML + PDF)"
	@echo "\tview-pdf\t\tto resulting PDF documentation"
	@echo "\tview-html\t\tto resulting HTML documentation"

.PHONY: release
release: config
	$(mySUDO) ./automate_release -n $(RELEASE)

#####################################################
owner:
	@echo "Re-own files to $(shell stat -c '%U' .)"
	chown --reference=. --from=root:root --preserve-root --recursive .

%-sign: $(IMAGE_FILES)
	$(MAKE) $(DISTRIBUTION_SIGNATURES_FILES)
	@echo "Signed files: $(DISTRIBUTION_SIGNATURES_FILES)"
sign: $(BRANCHE)-sign

clean-sign:
	-rm *.$(GPG_SIG_SUFFIX)

%.$(GPG_SIG_SUFFIX): %
	-$(mySUDO) gpg --yes --import-options import-clean --import $(CREDENTIALS_GPG_FILE)
	$(mySUDO) gpg --yes --batch --detach-sign --armor --local-user $(CREDENTIALS_GPG_FINGERPRINT) $<
	$(mySUDO) gpg --yes --batch --delete-secret-and-public-key $(CREDENTIALS_GPG_FINGERPRINT)
%-gco:
	$(mySUDO) git checkout $*

%-clean: clean-sign
	@echo "Clean up branch $*'s build"
	lb clean
	-rm -Rf .build/
clean: $(BRANCHE)-clean

%-clean-docs:
	@echo "Clean up branch $*'s documentations"
	-rm -Rf $(DOC_DIR)/.build
clean-docs: $(BRANCHE)-clean-docs

%-clean-dist:
	@echo "Clean up branch $*'s distribution"
	-rm -Rf $(DIST_DIR)/$*
clean-dist: $(BRANCHE)-clean-dist

%-purge: %-clean %-clean-dist %-clean-docs
	@echo "Purge branch $*"
	lb clean --purge
purge: $(BRANCHE)-purge

%-sanitize: %-gco owner
	@echo "Sanitize branch $*"
	$(mySUDO) git checkout -- .
sanitize: $(BRANCHE)-sanitize

%-config: %-sanitize
	@echo "Configure branch $*'s system"
	$(mySUDO) lb config
config: $(BRANCHE)-config

%-build: %-clean %-config
	@echo "Build branch $*'s system"
	lb build
build: $(BRANCHE)-build

%-uefi: $(wildcard liveUSB*.img)
	./burn --no-copy --no-persistence --no-crypt --image $<
uefi: $(BRANCHE)-uefi

%-dist-copy:
	@echo "Copy distribution files to $(DIST_DIR)/$*"
	@mkdir -p $(DIST_DIR)/$*/$(META_DIR)
	cp $(DISTRIBUTION_FILES) $(DISTRIBUTION_SIGNATURES_FILES) $(DIST_DIR)/$*
	cp $(DISTRIBUTION_META_FILES) $(DIST_DIR)/$*/$(META_DIR)
dist-copy: $(BRANCHE)-dist-copy

%-dist: %-build
	$(MAKE) $*-uefi
	$(mySUDO) $(MAKE) $*-sign
	$(mySUDO) $(MAKE) $*-dist-copy
dist: $(BRANCHE)-dist

%-do-pdf: $(DOCUMENTATION_REQUISITES) %-gco
	$(MAKE) latexpdf -C $(DOC_DIR)
pdf: $(BRANCHE)-do-pdf

%-view-pdf: %-do-pdf
	evince $(DOC_DIR)/.build/latex/*.pdf &
view-pdf: $(BRANCHE)-view-pdf

%-dist-pdf: %-do-pdf
	@mkdir -p $(DIST_DIR)/$*/$(DOC_DIR)
	cp $(DOC_DIR)/.build/latex/*.pdf $(DIST_DIR)/$*/$(DOC_DIR)
dist-pdf: $(BRANCHE)-dist-pdf

%-do-html: $(DOCUMENTATION_REQUISITES) %-gco
	$(MAKE) html -C $(DOC_DIR)
html: $(BRANCHE)-do-html

%-view-html: $(DOCUMENTATION_REQUISITES) %-do-html
	@xdg-open $(DOCUMENTATION_HTML_FILE)
view-html: $(BRANCHE)-view-html

%-dist-html: %-do-html
	@mkdir -p $(DIST_DIR)/$*/$(DOC_DIR)
	cp -r $(DOCUMENTATION_HTML_DIR) $(DIST_DIR)/$*/$(DOC_DIR)
dist-html: $(BRANCHE)-dist-html

%-dist-docs: %-clean-docs %-dist-pdf %-dist-html
	@echo "Placed $* documentation files in $(DIST_DIR)/$*"
dist-docs: $(BRANCHE)-dist-docs

.ONESHELL:
SHELL = /bin/bash
%-do-upload:
	source $(CREDENTIALS_FTP_FILE)
	cd $(DIST_DIR)/$* && find . -depth -type f -print0 | xargs -0 -I '{}' curl --netrc --netrc-file $(shell pwd)/$(CREDENTIALS_NETRC_FILE) --ftp-create-dirs --upload-file '{}' $$REMOTE_FTP_HOST/$*/'{}'
upload: $(BRANCHE)-do-upload

dist-upload: .-do-upload

.ONESHELL:
SHELL = /bin/bash
%-do-rsync:
	source $(CREDENTIALS_RSYNC_FILE)
	rsync --progress --update --recursive dist/$* $$REMOTE_SSH_USER@$$REMOTE_SSH_HOST:$$REMOTE_SSH_PATH/$*
rsync: $(BRANCHE)-do-rsync

dist-rsync: .-do-rsync
